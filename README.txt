For full details on how this works, see the documentation in the help 
directory or at /admin/help/file_ownage

This module uses the watchdog as its own feedback mechanism.
Extensive action descriptions are sent to the reports logs, so review that for
step-by-step debugging.

By default anything at severity WATCHDOG_NOTICE or higher gets echoed to 
the screen.
That level indicates actual write-back changes that are being made to your 
content.
Turn the volume down on the settings page at /admin/settings/file_ownage
